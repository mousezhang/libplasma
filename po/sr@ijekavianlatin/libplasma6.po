# Translation of libplasma5.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017.
# Dalibor Djuric <daliborddjuric@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: libplasma5\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-13 00:39+0000\n"
"PO-Revision-Date: 2016-03-20 15:24+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavianlatin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: declarativeimports/plasmaextracomponents/qml/BasicPlasmoidHeading.qml:81
#, kde-format
msgid "More actions"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:516
#, kde-format
msgctxt "@action:button"
msgid "Collapse"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:516
#, kde-format
msgctxt "@action:button"
msgid "Expand"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/PasswordField.qml:49
#, kde-format
msgid "Password"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:60
#, kde-format
msgid "Search…"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:62
#, kde-format
msgid "Search"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:73
#, kde-format
msgid "Clear search"
msgstr ""

#: plasma/applet.cpp:300
#, kde-format
msgid "Unknown"
msgstr "Nepoznato"

#: plasma/applet.cpp:743
#, kde-format
msgid "Activate %1 Widget"
msgstr "Aktiviraj vidžet %1"

#: plasma/containment.cpp:97 plasma/private/applet_p.cpp:108
#, fuzzy, kde-format
#| msgctxt "%1 is the name of the applet"
#| msgid "Remove this %1"
msgctxt "%1 is the name of the applet"
msgid "Remove %1"
msgstr ""
"Ukloni ovaj %1|/|Ukloni $[po-rodu-broju %1 ovaj ovu ovo ove ove ova] $[aku "
"%1]"

#: plasma/containment.cpp:103 plasma/corona.cpp:367 plasma/corona.cpp:485
#, kde-format
msgid "Enter Edit Mode"
msgstr ""

#: plasma/containment.cpp:106 plasma/private/applet_p.cpp:113
#, kde-format
msgctxt "%1 is the name of the applet"
msgid "Configure %1..."
msgstr ""

#: plasma/corona.cpp:316 plasma/corona.cpp:470
#, kde-format
msgid "Lock Widgets"
msgstr "Zaključaj vidžete"

#: plasma/corona.cpp:316
#, kde-format
msgid "Unlock Widgets"
msgstr "Otključaj vidžete"

#: plasma/corona.cpp:365
#, kde-format
msgid "Exit Edit Mode"
msgstr ""

#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:9
#, kde-format
msgid "Whether or not to create an on-disk cache for the theme."
msgstr "Da li napraviti keš na disku za temu."

# skip-rule: t-setting
#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:14
#, kde-format
msgid ""
"The maximum size of the on-disk Theme cache in kilobytes. Note that these "
"files are sparse files, so the maximum size may not be used. Setting a "
"larger size is therefore often quite safe."
msgstr ""
"Najveća veličina keša teme na disku u kilobajtima. Imajte na umu da su ovi "
"fajlovi retki, tako da zadata veličina ne mora biti potpuno iskorišćena. "
"Zato je zadavanje veće vrednosti često sasvim bezbedno."

#: plasma/private/applet_p.cpp:138
#, kde-format
msgctxt "Package file, name of the widget"
msgid "Could not open the %1 package required for the %2 widget."
msgstr "Ne mogu da otvorim paket %1 neophodan za vidžet %2."

#: plasma/private/applet_p.cpp:146
#, fuzzy, kde-format
#| msgid "Alternatives..."
msgid "Show Alternatives..."
msgstr "Alternative..."

#: plasma/private/applet_p.cpp:253
#, kde-format
msgid "Widget Removed"
msgstr "Vidžet uklonjen"

#: plasma/private/applet_p.cpp:254
#, kde-format
msgid "The widget \"%1\" has been removed."
msgstr "Vidžet „%1“ je uklonjen."

#: plasma/private/applet_p.cpp:258
#, kde-format
msgid "Panel Removed"
msgstr "Panel uklonjen"

#: plasma/private/applet_p.cpp:259
#, kde-format
msgid "A panel has been removed."
msgstr "Panel je uklonjen."

#: plasma/private/applet_p.cpp:262
#, kde-format
msgid "Desktop Removed"
msgstr "Površ uklonjena"

#: plasma/private/applet_p.cpp:263
#, kde-format
msgid "A desktop has been removed."
msgstr "Površ je uklonjena."

#: plasma/private/applet_p.cpp:266
#, kde-format
msgid "Undo"
msgstr "Opozovi"

#: plasma/private/applet_p.cpp:357
#, kde-format
msgid "Widget Settings"
msgstr "Postavke vidžeta"

#: plasma/private/applet_p.cpp:364
#, kde-format
msgid "Remove this Widget"
msgstr "Ukloni ovaj vidžet"

#: plasma/private/containment_p.cpp:58
#, kde-format
msgid "Remove this Panel"
msgstr "Ukloni ovaj panel"

#: plasma/private/containment_p.cpp:60
#, kde-format
msgid "Remove this Activity"
msgstr "Ukloni ovu aktivnost"

#: plasma/private/containment_p.cpp:66
#, kde-format
msgid "Activity Settings"
msgstr "Postavke aktivnosti"

#: plasma/private/containment_p.cpp:78
#, kde-format
msgid "Add Widgets..."
msgstr "Dodaj vidžete..."

#: plasma/private/containment_p.cpp:197
#, kde-format
msgid "Could not find requested component: %1"
msgstr "Ne mogu da nađem zahtijevanu komponentu: %1"

#. i18n: ectx: property (text), widget (QLabel, label)
#: plasma/private/publish.ui:17
#, kde-format
msgid ""
"Sharing a widget on the network allows you to access this widget from "
"another computer as a remote control."
msgstr ""
"Dijeljenje vidžeta na mreži omogućava vam da pristupite tom vidžetu sa "
"drugog računara kao daljinskim upravljačem."

#. i18n: ectx: property (text), widget (QCheckBox, publishCheckbox)
#: plasma/private/publish.ui:27
#, kde-format
msgid "Share this widget on the network"
msgstr "Podijeli ovaj vidžet na mreži"

#. i18n: ectx: property (text), widget (QCheckBox, allUsersCheckbox)
#: plasma/private/publish.ui:37
#, kde-format
msgid "Allow everybody to freely access this widget"
msgstr "Dozvoli svima slobodan pristup ovom vidžetu"

#: plasma/private/service_p.h:32
#, kde-format
msgctxt "Error message, tried to start an invalid service"
msgid "Invalid (null) service, can not perform any operations."
msgstr "Neispravan (nulti) servis, ne može izvršiti nikakvu operaciju."

#: plasmaquick/appletquickitem.cpp:519
#, kde-format
msgid "The root item of %1 must be of type ContainmentItem"
msgstr ""

#: plasmaquick/appletquickitem.cpp:524
#, kde-format
msgid "The root item of %1 must be of type PlasmoidItem"
msgstr ""

#: plasmaquick/appletquickitem.cpp:531
#, fuzzy, kde-format
#| msgid "Unknown"
msgid "Unknown Applet"
msgstr "Nepoznato"

#: plasmaquick/appletquickitem.cpp:544
#, kde-format
msgid ""
"This Widget was written for an unknown older version of Plasma and is not "
"compatible with Plasma %1. Please contact the widget's author for an updated "
"version."
msgstr ""

#: plasmaquick/appletquickitem.cpp:550
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please contact the widget's author for an updated version."
msgstr ""

#: plasmaquick/appletquickitem.cpp:555
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please update Plasma in order to use the widget."
msgstr ""

#: plasmaquick/appletquickitem.cpp:574
#, fuzzy, kde-format
#| msgid "Error loading QML file: %1"
msgid "Error loading QML file: %1 %2"
msgstr "Greška pri učitavanju QML fajla: %1"

#: plasmaquick/appletquickitem.cpp:577
#, fuzzy, kde-format
#| msgid "Error loading Applet: package inexistent. %1"
msgid "Error loading Applet: package does not exist. %1"
msgstr "Greška pri učitavanju apleta: paket ne postoji. %1"

# >> %1 is provider name
#: plasmaquick/configview.cpp:236
#, fuzzy, kde-format
#| msgid "%1 Settings"
msgid "%1 — %2 Settings"
msgstr "Postavke %1|/|Postavke $[gen %1]"

# >> %1 is provider name
#: plasmaquick/configview.cpp:237
#, kde-format
msgid "%1 Settings"
msgstr "Postavke %1|/|Postavke $[gen %1]"

# >> @title
#: plasmaquick/plasmoid/containmentitem.cpp:540
#, kde-format
msgid "Plasma Package"
msgstr "Plasma paket"

#: plasmaquick/plasmoid/containmentitem.cpp:544
#, kde-format
msgid "Install"
msgstr "Instaliraj"

#: plasmaquick/plasmoid/containmentitem.cpp:555
#, kde-format
msgid "Package Installation Failed"
msgstr "Propalo instaliranje paketa"

#: plasmaquick/plasmoid/containmentitem.cpp:571
#, kde-format
msgid "The package you just dropped is invalid."
msgstr "Paket koji ste upravo ispustili nije dobar."

# >> @title
#: plasmaquick/plasmoid/containmentitem.cpp:580
#: plasmaquick/plasmoid/containmentitem.cpp:649
#, kde-format
msgid "Widgets"
msgstr "Vidžeti"

#: plasmaquick/plasmoid/containmentitem.cpp:585
#, kde-format
msgctxt "Add widget"
msgid "Add %1"
msgstr ""

# >> @action
#: plasmaquick/plasmoid/containmentitem.cpp:599
#: plasmaquick/plasmoid/containmentitem.cpp:653
#, fuzzy, kde-format
#| msgid "Icon"
msgctxt "Add icon widget"
msgid "Add Icon"
msgstr "Ikonica"

# >> @title
#: plasmaquick/plasmoid/containmentitem.cpp:611
#, kde-format
msgid "Wallpaper"
msgstr "Tapet"

#: plasmaquick/plasmoid/containmentitem.cpp:621
#, kde-format
msgctxt "Set wallpaper"
msgid "Set %1"
msgstr ""

#: plasmaquick/plasmoid/dropmenu.cpp:28
#, kde-format
msgid "Content dropped"
msgstr "Sadržaj prevučen"

#~ msgid "The %1 widget did not define which ScriptEngine to use."
#~ msgstr "Vidžet %1 ne definiše koji skriptni motor treba za njega."

# >> @item applet category
#~ msgctxt "misc category"
#~ msgid "Miscellaneous"
#~ msgstr "razno"

# >> @item:intext
#~ msgid "Main Script File"
#~ msgstr "glavni fajl skripte"

# >> @item:intext
#~ msgid "Tests"
#~ msgstr "probe"

# >> @item file/directory definition
#~ msgid "Images"
#~ msgstr "slike"

# >> @item file/directory definition
#~ msgid "Themed Images"
#~ msgstr "tematske slike"

# >> @item file/directory definition
#~ msgid "Configuration Definitions"
#~ msgstr "definicije postave"

# >> @item file/directory definition
#~ msgid "User Interface"
#~ msgstr "korisničko sučelje"

# >> @item file/directory definition
#~ msgid "Data Files"
#~ msgstr "podaci"

# >> @item file/directory definition
#~ msgid "Executable Scripts"
#~ msgstr "izvršne skripte"

# >> @item file/directory definition
#~ msgid "Screenshot"
#~ msgstr "snimak ekrana"

# >> @item file/directory definition
#~ msgid "Translations"
#~ msgstr "prevodi"

# >> @item file/directory definition
#~ msgid "Configuration UI pages model"
#~ msgstr "model UI stranica postave"

# >> @item file/directory definition
#~ msgid "Configuration XML file"
#~ msgstr "XML fajl postave"

# >> @item file/directory definition
#~ msgid "Custom expander for compact applets"
#~ msgstr "posebni proširivač za sažete aplete"

# >> @item file/directory definition
#~ msgid "Images for dialogs"
#~ msgstr "slike za dijaloge"

# >> @item file/directory definition
#~ msgid "Generic dialog background"
#~ msgstr "generička pozadina dijaloga"

# >> @item file/directory definition
#~ msgid "Theme for the logout dialog"
#~ msgstr "tema za odjavni dijalog"

# >> @item file/directory definition
#~ msgid "Wallpaper packages"
#~ msgstr "paketi tapeta"

# >> @item file/directory definition
#~ msgid "Images for widgets"
#~ msgstr "slike za vidžete"

# >> @item file/directory definition
#~ msgid "Background image for widgets"
#~ msgstr "pozadinska slika za vidžete"

# >> @item file/directory definition
#~ msgid "Analog clock face"
#~ msgstr "lice analognog sata"

# >> @item file/directory definition
#~ msgid "Background image for panels"
#~ msgstr "pozadinska slika za panele"

# >> @item file/directory definition
#~ msgid "Background for graphing widgets"
#~ msgstr "pozadina za crtačke vidžete"

# >> @item file/directory definition
#~ msgid "Background image for tooltips"
#~ msgstr "pozadinska slika za oblačiće"

# >> @item file/directory definition
#~ msgid "Opaque images for dialogs"
#~ msgstr "neprozirne slike za dijaloge"

# >> @item file/directory definition
#~ msgid "Opaque generic dialog background"
#~ msgstr "neprozirna generička pozadina dijaloga"

# >> @item file/directory definition
#~ msgid "Opaque theme for the logout dialog"
#~ msgstr "neprozirna tema za odjavni dijalog"

# >> @item file/directory definition
#~ msgid "Opaque images for widgets"
#~ msgstr "neprozirne slike za vidžete"

# >> @item file/directory definition
#~ msgid "Opaque background image for panels"
#~ msgstr "neprozirna pozadinska slika za panele"

# >> @item file/directory definition
#~ msgid "Opaque background image for tooltips"
#~ msgstr "neprozirna pozadinska slika za oblačiće"

# >> @item file/directory definition
#~ msgid "KColorScheme configuration file"
#~ msgstr "postavni fajl šeme boja"

# >> @item file/directory definition
#~ msgid "Service Descriptions"
#~ msgstr "opisi servisa"

#~ msgctxt ""
#~ "API or programming language the widget was written in, name of the widget"
#~ msgid "Could not create a %1 ScriptEngine for the %2 widget."
#~ msgstr "Ne mogu da stvorim skriptni motor %1 za vidžet %2."

#~ msgid "Script initialization failed"
#~ msgstr "Neuspjelo pripremanje skripte"

#~ msgctxt "Agenda listview section title"
#~ msgid "Holidays"
#~ msgstr "Praznici"

#~ msgctxt "Agenda listview section title"
#~ msgid "Events"
#~ msgstr "Događaji"

#~ msgctxt "Agenda listview section title"
#~ msgid "Todo"
#~ msgstr "Obaveze"

#~ msgctxt "Means 'Other calendar items'"
#~ msgid "Other"
#~ msgstr "Drugo"

#~ msgctxt "Reset calendar to today"
#~ msgid "Today"
#~ msgstr "Danas"

#~ msgid "Reset calendar to today"
#~ msgstr "Resetuj kalendar na danas"

#~ msgid "Previous Month"
#~ msgstr "Prethodni mesec"

#~ msgid "Next Month"
#~ msgstr "Naredni mesec"

#~ msgid "Previous Year"
#~ msgstr "Prethodna godina"

#~ msgid "Next Year"
#~ msgstr "Naredna godina"

#~ msgid "Previous Decade"
#~ msgstr "Prethodna decenija"

#~ msgid "Next Decade"
#~ msgstr "Naredna decenija"

#~ msgid "OK"
#~ msgstr "U redu"

#~ msgid "Cancel"
#~ msgstr "Odustani"

# >> @item applet category
#~ msgid "Accessibility"
#~ msgstr "pristupačnost"

# >> @item applet category
#~ msgid "Application Launchers"
#~ msgstr "pokretači programa"

# >> @item applet category
#~ msgid "Astronomy"
#~ msgstr "astronomija"

# >> @item applet category
#~ msgid "Date and Time"
#~ msgstr "datum i vreme"

# >> @item applet category
#~ msgid "Development Tools"
#~ msgstr "razvojne alatke"

# >> @item applet category
#~ msgid "Education"
#~ msgstr "obrazovanje"

# >> @item applet category
#~ msgid "Environment and Weather"
#~ msgstr "priroda i vrijeme"

# >> @item applet category
#~ msgid "Examples"
#~ msgstr "primeri"

# >> @item applet category
#~ msgid "File System"
#~ msgstr "fajl sistem"

# >> @item applet category
#~ msgid "Fun and Games"
#~ msgstr "zabava i igre"

# >> @item applet category
#~ msgid "Graphics"
#~ msgstr "grafika"

# >> @item applet category
#~ msgid "Language"
#~ msgstr "jezik"

# >> @item applet category
#~ msgid "Mapping"
#~ msgstr "kartografija"

# >> @item applet category
#~ msgid "Miscellaneous"
#~ msgstr "razno"

# >> @item applet category
#~ msgid "Multimedia"
#~ msgstr "multimedija"

# >> @item applet category
#~ msgid "Online Services"
#~ msgstr "servisi na vezi"

# >> @item applet category
#~ msgid "Productivity"
#~ msgstr "produktivnost"

# >> @item applet category
#~ msgid "System Information"
#~ msgstr "podaci o sistemu"

# >> @item applet category
#~ msgid "Utilities"
#~ msgstr "alatke"

# >> @item applet category
#~ msgid "Windows and Tasks"
#~ msgstr "prozori i zadaci"

# >> @item applet category
#~ msgid "Clipboard"
#~ msgstr "klipbord"

# >> @item applet category
#~ msgid "Tasks"
#~ msgstr "zadaci"

#~ msgid "Run the Associated Application"
#~ msgstr "Izvrši pridruženi program"

#~ msgid "Open with %1"
#~ msgstr "Otvori pomoću %1|/|Otvori $[ins-p %1]"

# >> @item file/directory definition
#~ msgid "Default settings for theme, etc."
#~ msgstr "podrazumevane postavke za temu, itd."

# >> @item file/directory definition
#~ msgid "Color scheme to use for applications."
#~ msgstr "šema boja u programima"

# >> @item file/directory definition
#~ msgid "Preview Images"
#~ msgstr "slike pregleda"

# >> @item file/directory definition
#~ msgid "Preview for the Login Manager"
#~ msgstr "pregled za menadžer prijavljivanja"

# >> @item file/directory definition
#~ msgid "Preview for the Lock Screen"
#~ msgstr "pregled za zabravni ekran"

# >> @item file/directory definition
#~ msgid "Preview for the Userswitcher"
#~ msgstr "pregled za menjač korisnika"

# >> @item file/directory definition
#~ msgid "Preview for the Virtual Desktop Switcher"
#~ msgstr "pregled za menjač virtuelnih površi"

# >> @item file/directory definition
#~ msgid "Preview for Splash Screen"
#~ msgstr "pregled za uvodni ekran"

# >> @item file/directory definition
#~ msgid "Preview for KRunner"
#~ msgstr "pregled za K‑izvođač"

# >> @item file/directory definition
#~ msgid "Preview for the Window Decorations"
#~ msgstr "pregled za dekoracije prozora"

# >> @item file/directory definition
#~ msgid "Preview for Window Switcher"
#~ msgstr "pregled za menjač prozora"

# >> @item file/directory definition
#~ msgid "Login Manager"
#~ msgstr "menadžer prijavljivanja"

# >> @item file/directory definition
#~ msgid "Main Script for Login Manager"
#~ msgstr "glavna skript za menadžer prijavljivanja"

# >> @item file/directory definition
#~ msgid "Logout Dialog"
#~ msgstr "odjavni dijalog"

# >> @item file/directory definition
#~ msgid "Main Script for Logout Dialog"
#~ msgstr "glavna skripta za odjavni dijalog"

# >> @item file/directory definition
#~ msgid "Screenlocker"
#~ msgstr "zaključavač ekrana"

# >> @item file/directory definition
# rewrite-msgid: /Lock Screen/Screen Locker/
#~ msgid "Main Script for Lock Screen"
#~ msgstr "glavna skripta za zaključavač ekrana"

# >> @item file/directory definition
#~ msgid "UI for fast user switching"
#~ msgstr "sučelje za brzo menjanje korisnika"

# >> @item file/directory definition
#~ msgid "Main Script for User Switcher"
#~ msgstr "glavna skripta za menjač korisnika"

# >> @item file/directory definition
#~ msgid "Virtual Desktop Switcher"
#~ msgstr "menjač virtuelnih površi"

# >> @item file/directory definition
#~ msgid "Main Script for Virtual Desktop Switcher"
#~ msgstr "glavna skripta za menjač virtuelnih površi"

# >> @item file/directory definition
#~ msgid "On-Screen Display Notifications"
#~ msgstr "obaveštenja u ekranskom prikazu"

# >> @item file/directory definition
#~ msgid "Main Script for On-Screen Display Notifications"
#~ msgstr "glavna skripta za obaveštenja u ekranskom prikazu"

# >> @item file/directory definition
#~ msgid "Splash Screen"
#~ msgstr "uvodni ekran"

# >> @item file/directory definition
#~ msgid "Main Script for Splash Screen"
#~ msgstr "glavna skripta za uvodni ekran"

# >> @item file/directory definition
#~ msgid "KRunner UI"
#~ msgstr "sučelje K‑izvođača"

# >> @item file/directory definition
#~ msgid "Main Script KRunner"
#~ msgstr "glavna skripta K‑izvođača"

# >> @item file/directory definition
#~ msgid "Window Decoration"
#~ msgstr "dekoracija prozora"

# >> @item file/directory definition
#~ msgid "Main Script for Window Decoration"
#~ msgstr "glavna skripta za dekoraciju prozora"

# >> @item file/directory definition
#~ msgid "Window Switcher"
#~ msgstr "menjač prozora"

# >> @item file/directory definition
#~ msgid "Main Script for Window Switcher"
#~ msgstr "glavna skripta za menjač prozora"
